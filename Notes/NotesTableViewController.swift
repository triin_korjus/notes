//
//  NotesTableViewController.swift
//  Notes
//
//  Created by Triin on 13/02/2018.
//  Copyright © 2018 Triin. All rights reserved.
//

import UIKit

class NotesTableViewController: UITableViewController, NoteTableViewCellDelegate {

    var notes: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageView = UIImageView(image: #imageLiteral(resourceName: "fob_logo"))
        imageView.contentMode = .scaleAspectFit
        tableView.backgroundView = imageView
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : notes.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard indexPath.section == 1 else {
            return tableView.dequeueReusableCell(withIdentifier: "addNoteCell", for: indexPath)
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "noteCell", for: indexPath) as! NoteTableViewCell

        cell.delegate = self
        cell.setup(note: notes[indexPath.row])
        
        return cell
    }
    
    // MARK: - Table view delegate

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            notes.append("")
            //tableView.reloadSections([1], with: .none)
            tableView.reloadData()
        }
        
    }
    
    // MARK: - Note table view cell delegate
    
    func noteTableViewCellDidEditText(_ sender: NoteTableViewCell) {
        guard let editedIndexPath = tableView.indexPath(for: sender), notes.count > editedIndexPath.row else { return }
        
        notes[editedIndexPath.row] = sender.noteText
    }
    
    func noteTableViewCellDidRemove(_ sender: NoteTableViewCell) {
        guard let removedIndexPath = tableView.indexPath(for: sender) else { return }
        
        notes.remove(at: removedIndexPath.row)
        //tableView.reloadSections([1], with: .none)
        tableView.reloadData()
    }
}
