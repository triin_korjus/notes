//
//  NoteTableViewCell.swift
//  Notes
//
//  Created by Triin on 13/02/2018.
//  Copyright © 2018 Triin. All rights reserved.
//

import UIKit

protocol NoteTableViewCellDelegate: class {
    func noteTableViewCellDidEditText(_ sender: NoteTableViewCell)
    func noteTableViewCellDidRemove(_ sender: NoteTableViewCell)
}

class NoteTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var noteTextField: UITextField!
    var noteText: String!
    
    weak var delegate: NoteTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        noteTextField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setup(note: String) {
        noteText = note
        noteTextField.text = noteText
    }
    
    @IBAction func didEditNote(_ sender: Any) {
        noteText = noteTextField.text
        delegate?.noteTableViewCellDidEditText(self)
    }
    
    @IBAction func deleteNote(_ sender: Any) {
        delegate?.noteTableViewCellDidRemove(self)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.noteTextField.resignFirstResponder()
        return true
    }
    
}
